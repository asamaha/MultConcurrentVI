Observing response procedures are useful in the study of conditioned reinforcement because they allow for rates of conditioned and unconditioned reinforcement to be disassociated unlike in chain schedules, in which rates of conditioned and unconditioned reinforcement covary. However, such procedures are difficult to implement in clinical settings because they require the experimenter maintain several separate timers simultaneously (e.g., transitions between components, the duration of Sd presentation, reinforcer schedules, reinforcer access time, etc). This web-app does that.

### Citation
Samaha, A. L. (2018). A web-based platform to implement concurrent variable-interval schedules under multiple component mixed and multiple arrangements. [Computer program]. Tampa, FL. Retrieved from https://gitlab.com/asamaha/MultConcurrentVI

Click here to a [Live Demo](http://jstream.github.io/)

Return to the [Samaha Lab page](https://gitlab.com/asamaha/general/blob/master/README.md).